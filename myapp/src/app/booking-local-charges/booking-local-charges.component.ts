import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-booking-local-charges',
  templateUrl: './booking-local-charges.component.html',
  styleUrls: ['./booking-local-charges.component.scss']
})
export class BookingLocalChargesComponent implements OnInit {
  displayedColumns = ['name', 'basis', 'currency', 'roe', 'Rate', 'Tax'];
  displayedColumns2 = ['name', 'basis', 'currency', 'roe', 'Rate', 'Tax'];
  @Input() OfferOriginCharges: any[];
  @Input() OfferDestinationCharges: any[];

  constructor() { }

  ngOnInit(): void {
    console.log(this.OfferOriginCharges)
    console.log(this.OfferDestinationCharges)
  }

}
