import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingLocalChargesComponent } from './booking-local-charges.component';

describe('BookingLocalChargesComponent', () => {
  let component: BookingLocalChargesComponent;
  let fixture: ComponentFixture<BookingLocalChargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingLocalChargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingLocalChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
