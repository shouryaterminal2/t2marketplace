import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-booking-additional-services',
  templateUrl: './booking-additional-services.component.html',
  styleUrls: ['./booking-additional-services.component.scss']
})
export class BookingAdditionalServicesComponent implements OnInit {
  displayedColumns = ['service', 'desc'];
  displayedColumns2 = ['service', 'desc'];
  @Input() offerAdditionalServiceOrigin: any[];
  @Input() offerAdditionalServiceDestination:any[];

  constructor() { }

  ngOnInit(): void {
  }

}
