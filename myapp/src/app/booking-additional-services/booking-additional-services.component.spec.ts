import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingAdditionalServicesComponent } from './booking-additional-services.component';

describe('BookingAdditionalServicesComponent', () => {
  let component: BookingAdditionalServicesComponent;
  let fixture: ComponentFixture<BookingAdditionalServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingAdditionalServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingAdditionalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
